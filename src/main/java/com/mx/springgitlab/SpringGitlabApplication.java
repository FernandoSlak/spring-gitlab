package com.mx.springgitlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGitlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringGitlabApplication.class, args);
	}

}
